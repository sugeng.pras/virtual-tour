using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DroidHunt
{
    public class DH_Blocker : MonoBehaviour
    {
        [SerializeField] float moveSpeed;
        [SerializeField] float rotateSpeed;
        Vector3 curDestination;
        DH_GameManager gameManager;

        private void Awake()
        {
            gameManager = FindObjectOfType<DH_GameManager>();
        }
        // Update is called once per frame
        void Update()
        {
            MoveToward(curDestination);
            transform.Rotate(0, 0, rotateSpeed);

            if (Vector2.Distance(transform.position, curDestination) < 0.5f)
            {
                GetDestination();
            }
        }
        void GetDestination()
        {
            Bounds area = gameManager.cameraBound;

            int randomPosX = Mathf.FloorToInt(Random.Range(area.min.x, area.max.x));
            int randomPosY = Mathf.FloorToInt(Random.Range(area.min.y, area.max.y));

            curDestination = new Vector3(randomPosX, randomPosY, -1);


        }

        void MoveToward(Vector3 destination)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * moveSpeed);
        }

    }
}
