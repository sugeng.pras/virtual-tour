using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DroidHunt
{
    public class DH_ObstacleControl : VtourMinigames.BaseObstacle, DH_IShootTarget
    {
        [SerializeField] float obstacleSpeed;
        [SerializeField] Transform sprite;
        DH_GameManager gameManager;
        bool isReady = false;


        Vector3 curDestination;

        private void Update()
        {
            if (isReady)
            {
                MoveToward(curDestination);

                if (Vector2.Distance(transform.position, curDestination) < 0.5f)
                {
                    GetDestination();
                }
            }
        }

        private void OnEnable()
        {
            if (gameManager == null) gameManager = FindObjectOfType<DH_GameManager>();
            GetDestination();

        }
        public void HitByRay()
        {
            isReady = false;
            gameManager.AddScore(10);
            controller.StoreHidenObject(this.gameObject);
        }

        void GetDestination()
        {
            isReady = true;
            Bounds area = gameManager.cameraBound;

            int randomPosX = Mathf.FloorToInt(Random.Range(area.min.x, area.max.x));
            int randomPosY = Mathf.FloorToInt(Random.Range(area.min.y, area.max.y));

            curDestination = new Vector3(randomPosX, randomPosY, 0);

            transform.localEulerAngles = new Vector3(0, 0,
                (Mathf.Atan2(curDestination.y - transform.position.y,curDestination.x - transform.position.x) * 180 / Mathf.PI)-90) ;
        }

        void MoveToward(Vector3 destination)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * obstacleSpeed);
        }
    }
}
