using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DroidHunt
{
    public class DH_RunnerObstacle : MonoBehaviour, DH_IShootTarget
    {

        [SerializeField] BoxCollider hitCollider;

        [SerializeField] DH_GameManager gameManager;
        [SerializeField] Transform[] destPosition;
        VtourMinigames.UnitAnimatorControl animControl;

        [SerializeField] GameObject unitObject;
        Vector3 originScale;
        //[SerializeField] SpriteRenderer charaSprite;
        int curTargetIndex = 0;

        bool moveUp = false, moveDown = false;
        float originY;
        [SerializeField] float maxHeight = 3;
        [SerializeField] float speed = 2;

        private void Awake()
        {
            originScale = unitObject.transform.localScale;
            animControl = new VtourMinigames.UnitAnimatorControl(GetComponentInChildren<Animator>());
            originY = transform.position.y;
            InvokeRepeating("StartJump", 2, 3);
            animControl.RequestAnim("Run");
        }

        private void Update()
        {
            MoveToward();

            if (moveUp) MoveUp();
            else if (moveDown) MoveDown();
        }

        void MoveToward()
        {
            unitObject.transform.localScale = new Vector3(curTargetIndex % 2 == 1 ? -originScale.x : originScale.x,
               originScale.y, originScale.z);

            if (moveDown && transform.position.y <0.2f) animControl.RequestAnim("Run");

            transform.position = Vector3.MoveTowards(transform.position, destPosition[curTargetIndex % 2].position, Time.deltaTime * speed);
       

            if (Vector3.Distance(transform.position, destPosition[curTargetIndex % 2].position) < 0.1)
            {
                curTargetIndex++;
                MoveToward();
                SetActive(true);
            }
        }

        void StartJump()
        {
            if (gameObject.activeSelf)
            {
                StartCoroutine(JumpPhase());
            }
        }
        void MoveUp()
        {
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, maxHeight, 2 * Time.deltaTime), 2);
        }
        void MoveDown()
        {
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, originY, 2 * Time.deltaTime), 2);
        }

        void SetActive(bool isActive)
        {
            hitCollider.enabled = isActive;
            unitObject.SetActive(isActive);
        }

        IEnumerator JumpPhase()
        {
            moveUp = true;
            moveDown = false;
            animControl.RequestAnim("Jump");

            yield return new WaitForSeconds(1);
            moveUp = false;
            moveDown = true;
        }

        public void HitByRay()
        {
            SetActive(false);
            gameManager.AddScore(30);

        }
    }
}
