using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DroidHunt{
    public interface DH_IShootTarget
    {
        void HitByRay();
    }
}