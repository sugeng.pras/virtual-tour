using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace DroidHunt
{
    public class DH_GameManager : VtourMinigames.BaseMiniGameManager
    {
        [SerializeField] float gamePlayTime;
        [SerializeField] TextMeshProUGUI timeText;

        protected override int GameID() => 3;

        public Bounds cameraBound { private set; get; }

        private void Update()
        {
            if (isGameStart)
            {
                gamePlayTime -= Time.deltaTime;
                if(gamePlayTime <= 0)
                {
                    gamePlayTime = 0;
                    GameOver();
                }
                timeText.text = gamePlayTime.ToString(".##");
            }
        }

        private void OnEnable()
        {
            cameraBound = Camera.main.OrthographicBounds();
            GameStart();
        }
        public void AddScore(int scoreAmount)
        {
            curentScore += scoreAmount;

            text_curentScore.text = curentScore.ToString();
        }
    }


    public static class CameraExtensions
    {
        public static Bounds OrthographicBounds(this Camera camera)
        {
            float screenAspect = (float)Screen.width / (float)Screen.height;
            float cameraHeight = camera.orthographicSize * 2;
            Bounds bounds = new Bounds(
                camera.transform.position,
                new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
            return bounds;
        }
    }
}
