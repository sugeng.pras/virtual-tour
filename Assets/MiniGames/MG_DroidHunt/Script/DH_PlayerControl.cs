using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DroidHunt
{
    public class DH_PlayerControl : MonoBehaviour
    {
        Camera mainCam;
        DH_GameManager gameManager;

        [SerializeField] Transform crossImage;
        [SerializeField] Canvas crossCanvas;
        private void Awake()
        {
            gameManager = FindObjectOfType<DH_GameManager>();
            mainCam = Camera.main;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) && gameManager.isGameStart)
            {
                Shoot(SetRayPos());
            }
            SetCrosshairPos();
        }

        void Shoot(Vector3 beginPos)
        {
            Ray ray = new Ray(beginPos, Vector3.forward);

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                if (hit.collider.tag == "obstacle" || hit.collider.tag == "powerUp")
                {
                    if (hit.collider.TryGetComponent<DH_IShootTarget>(out DH_IShootTarget obstacle))
                    {
                        obstacle.HitByRay();
                    }
                }
            }
        }

        Vector3 SetRayPos()
        {
            Vector3 pos = mainCam.ScreenToWorldPoint(Input.mousePosition);
            return new Vector3(pos.x, pos.y, -10);
        }

        void SetCrosshairPos()
        {
            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(crossCanvas.transform as RectTransform, Input.mousePosition, crossCanvas.worldCamera, out pos);
            crossImage.position = crossCanvas.transform.TransformPoint(pos);
        }

    }
}
