using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VtourMinigames
{
    public class FollowingTarget : MonoBehaviour
    {
        public GameObject followedTarget;
        public bool freezeX; public bool freezeY;

        Vector3 originPos;
        Vector3 distance;

        private void Update()
        {
            FollowTarget();
        }
        private void Awake()
        {
            originPos = transform.position;
            distance = transform.position - followedTarget.transform.position;
        }

        void FollowTarget()
        {
            transform.position = new Vector3(
                freezeX ? transform.position.x : followedTarget.transform.position.x + distance.x,
                freezeY ? transform.position.y : followedTarget.transform.position.y + distance.y,
                0
                );
        }
    }
}
