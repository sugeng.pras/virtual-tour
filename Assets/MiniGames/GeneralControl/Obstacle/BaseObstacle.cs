using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VtourMinigames
{
    public class BaseObstacle : MonoBehaviour, IEndZoneReciver
    {
        protected CuntinousObstacleGenerator controller;
        public virtual void Initial(CuntinousObstacleGenerator controller)
        {
            this.controller = controller;
        }

        public virtual void ReachEndZone()
        {
            controller.StoreHidenObject(this.gameObject);
            this.gameObject.SetActive(false);
        }
    }
}
