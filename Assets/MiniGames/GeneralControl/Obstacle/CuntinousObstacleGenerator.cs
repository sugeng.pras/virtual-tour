using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VtourMinigames
{
    public class CuntinousObstacleGenerator : MonoBehaviour
    {
        [SerializeField]
        BaseMiniGameManager manager;

        [SerializeField] protected BaseObstacle[] obstaclePerf;
        [SerializeField] protected Transform obstaclePlace;

        [Header("Pooling Setting")]
        [SerializeField] protected int poolObjectLimit = 10;
        protected List<GameObject> createdObstacle = new List<GameObject>();
        List<GameObject> hideObject = new List<GameObject>();

        [SerializeField] protected Transform[] summonPlaces;

        [Header("Summon Conntrol Time")]
        [SerializeField] protected float summonRateTime;
        [SerializeField] float delayBeforeSpawn;

        float runTime;
        float curSumTime;

        private void Update()
        {
            if (manager.isGameStart)
            {
                if (delayBeforeSpawn <= 0)
                {
                    if (curSumTime <= 0)
                    {
                        CreateObstacle();
                        curSumTime = summonRateTime;
                    }
                    else
                    {
                        curSumTime -= Time.deltaTime;
                    }
                }
                else
                {
                    delayBeforeSpawn -= Time.deltaTime;
                }
            }

        }

        public void StoreHidenObject(GameObject target)
        {
            target.SetActive(false);
            hideObject.Add(target);
        }
        protected virtual void CreateObstacle()
        {

            if (createdObstacle.Count <= poolObjectLimit)
            {
                int index = obstaclePerf.Length != 1 ? Random.Range(0, obstaclePerf.Length) : 0;

                BaseObstacle curent = Instantiate(obstaclePerf[index]);

                curent.transform.position = GetSummonPlace();

                curent.Initial(this);
                curent.transform.SetParent(obstaclePlace, true);

                createdObstacle.Add(curent.gameObject);
            }
            else
            {
                ReSetObject();
            }


        }

        protected void ReSetObject()
        {
            if (hideObject.Count > 0)
            {
                GameObject target = hideObject[0];
                target.transform.position = GetSummonPlace();
                target.SetActive(true);
                hideObject.Remove(target);
            }
        }

       protected virtual Vector3 GetSummonPlace()
        {
            int placeIndex = Random.Range(0, summonPlaces.Length);

            return new Vector3(
                summonPlaces[placeIndex].position.x,
                summonPlaces[placeIndex].position.y,
                0);

        }

    }
}
