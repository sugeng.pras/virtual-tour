using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace VtourMinigames
{
    public class BaseMiniGameManager : MonoBehaviour
    {
        [Header("HUD")]
        [SerializeField] protected TextMeshProUGUI text_curentScore;
        [SerializeField] protected GameObject uiLoseGame;
        [SerializeField] protected GameObject beginUI;

        public bool isGameStart { private set; get; }

        [SerializeField] protected int curentScore;

        public virtual void GameStart()
        {
            isGameStart = true;
        }
        public virtual void GameOver()
        {
            isGameStart = false;
            uiLoseGame.SetActive(true);
            if (UserData.scoreGame[GameID()] < curentScore)
            {
                SQLControl.StoreGameResult(GameID(),
                UserData.scoreGame[GameID()] < curentScore ? curentScore : UserData.scoreGame[GameID()]);

                UserData.scoreGame[GameID()] = curentScore;
            }

        }
        protected virtual int GameID() => -1;

        #region Button Action
        public void Restart()
        {
            SceneLoader.ReloadScene(GameID() + 3);
        }
        public void ExitGame()
        {
            SceneLoader.UnloadScene(GameID() + 3, delegate
              {
                  GlobalGameManager.ChangeGameState(GameState.vtour);
              });
        }
        #endregion
    }
}
