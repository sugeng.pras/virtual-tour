using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace VtourMinigames
{
    public class UnitAnimatorControl
    {
        Animator anim;
        string curentState;

        public UnitAnimatorControl(Animator anim)
        {
            this.anim = anim;
        }

        public void RequestAnim (string stateName)
        {
            if(curentState != stateName)
            {
                anim.Play(stateName);
                curentState = stateName;
            }
           
        }
    }
}
