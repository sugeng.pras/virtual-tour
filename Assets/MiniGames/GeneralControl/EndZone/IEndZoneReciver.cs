using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace VtourMinigames
{
    public interface IEndZoneReciver
    {
        void ReachEndZone();
    }
}
