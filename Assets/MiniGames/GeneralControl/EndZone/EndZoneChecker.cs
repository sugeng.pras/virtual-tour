using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VtourMinigames
{

    public class EndZoneChecker : MonoBehaviour
    {
        private void OnTriggerExit(Collider other)
        {
            other.TryGetComponent<IEndZoneReciver>(out IEndZoneReciver target);
            if (target != null) target.ReachEndZone();
        }
    }
}
