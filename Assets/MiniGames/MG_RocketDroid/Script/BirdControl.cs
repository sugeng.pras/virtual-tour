using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AngryBird
{
    public class BirdControl : MonoBehaviour
    {
        [SerializeField] Rigidbody rigid;
        public AB_GameControl gameControl;

        public void AddForce(Vector3 dir, float shootPower)
        {
            rigid.isKinematic = false;
            rigid.AddForce(dir.normalized * shootPower, ForceMode.VelocityChange);
        }

        public void GetBack()
        {
            rigid.isKinematic = true;
        }

    }
}
