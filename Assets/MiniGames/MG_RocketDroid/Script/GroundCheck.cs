using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AngryBird
{
    public class GroundCheck : MonoBehaviour
    {
        public AB_GameControl gameControl;

        // Start is called before the first frame update
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "obstacle" && gameControl.isGameStart)
            {
                if (collision.contactCount > 0)
                {
                    Destroy(collision.gameObject);
                }
            }
        }
    }
}
