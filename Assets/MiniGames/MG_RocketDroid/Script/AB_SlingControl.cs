using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace AngryBird
{
    public class AB_SlingControl : MonoBehaviour
    {
        AB_GameControl gameManager;

        public BirdControl bird;

        [Header("Sling Properti")]
        [SerializeField] float shootPower;
        [SerializeField] float pullDistance;
        [SerializeField] Trajectory traject;

        [Header("Sling Action Point")]
        [SerializeField] Transform slingCenter;
        [SerializeField] Transform pullPoint;
        [SerializeField] Transform birdPlace;

        [Header("Status")]
        public bool birdGrab;

        [Header("Other")]
        Camera mainCam;

        private void Awake()
        {
            gameManager = FindObjectOfType<AB_GameControl>();
            mainCam = Camera.main;
        }
        #region Sling Action State

        private void Update()
        {
            if (gameManager.shootAmount > 0)
            {
                if (birdGrab)
                {
                    PullBird();
                }

                if (Input.GetMouseButtonDown(0))
                {
                    GrabBrid();
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    ReleaseBird();
                }
            }
        }

        // Grab
        void GrabBrid()
        {

            birdGrab = true;
            bird.GetBack();
            bird.transform.position = birdPlace.transform.position;

        }
        // Pull Birb
        void PullBird()
        {
            Vector3 touchPos = GetMousePos();
            Vector3 newPos = touchPos;
            Vector3 offset = newPos - slingCenter.position;

            pullPoint.position = slingCenter.position + Vector3.ClampMagnitude(offset, pullDistance);

            birdPlace.localEulerAngles = new Vector3(0, 0,
                Mathf.Atan2(pullPoint.position.y - birdPlace.position.y, pullPoint.position.x - birdPlace.position.x) * 180 / Mathf.PI);

            traject.velocity = shootPower;

        }
        //Release Birb
        void ReleaseBird()
        {
            birdGrab = false;
            Vector3 dir = (slingCenter.position - pullPoint.position);
            bird.AddForce(dir, shootPower);
            gameManager.shootAmount--;
            gameManager.UpdateBallAmout();

            if (gameManager.shootAmount == 0 && gameManager.obstacleAmount > 0)
            {
                gameManager.CallEndGame();
            }

        }

        #endregion

        Vector3 GetMousePos()
        {
            Vector3 pos = mainCam.ScreenToWorldPoint(Input.mousePosition);
            return new Vector3(pos.x, pos.y, 0);
        }
    }
}
