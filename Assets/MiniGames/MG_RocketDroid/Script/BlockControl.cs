using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AngryBird
{
    public class BlockControl : MonoBehaviour
    {


        AB_GameControl gameControl;

        private void OnEnable()
        {
            gameControl = FindObjectOfType<AB_GameControl>();
            gameControl.ObstacleCreated();
        }

        private void OnDestroy()
        {
            gameControl.ObstacleDestroyed();
            gameControl.AddScore(10);
        }

        int health = 4;
        private void OnCollisionEnter(Collision collision)
        {
            if (gameControl.isGameStart && (collision.gameObject.tag == "obstacle" || collision.gameObject.tag == "ball"))
            {
                Debug.Log("Get Hit");
                health -= 1;
                if (health <= 0)
                {
                    Destroy(this.gameObject);

                }
            }
        }
    }
}
