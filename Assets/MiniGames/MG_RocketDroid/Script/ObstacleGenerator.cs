using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AngryBird
{
    public class ObstacleGenerator : MonoBehaviour
    {
        [SerializeField] ObstacleControl[] type1Obstacle;
        [SerializeField] List<Transform> defaultNextSumPos;
        [SerializeField] List<Transform> nextSumPos;
        [SerializeField] Transform obstaclePlace;

        [SerializeField] int limitObject;

        AB_GameControl gameControl;

        private void Awake()
        {
            gameControl = FindObjectOfType<AB_GameControl>();
        }
        private void Start()
        {
            nextSumPos = new List<Transform>(defaultNextSumPos);
            CreateObstacle();
        }
        void CreateObstacle()
        {
            for (int i = 0; i < limitObject; i++)
            {
                GenerateObeject();
            }
            gameControl.StartGame();
        }
        void GenerateObeject()
        {
            int ran = Random.Range(0, type1Obstacle.Length);
            int ran_pos = Random.Range(0, nextSumPos.Count);

            ObstacleControl perf = Instantiate(type1Obstacle[ran], obstaclePlace, true);
            perf.transform.position = nextSumPos[ran_pos].position;

            nextSumPos.RemoveAt(ran_pos);
            nextSumPos.Add(perf.nextObstaclePos);

            perf.transform.DetachChildren();
        }

        public void GenerateAgain()
        {
            nextSumPos = new List<Transform>(defaultNextSumPos);
            Invoke("CreateObstacle", 1);
            gameControl.UnStartGame();
        }
    }
}
