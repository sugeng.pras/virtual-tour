using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace AngryBird
{

    public class AB_GameControl : MonoBehaviour
    {
        public int obstacleAmount;
        public int shootAmount;

        public bool isGameStart { private set; get; }
        const int gameID = 1;

        int score;
        int multipleAmount;

        bool multiple;
        float multipleDuration = 4;
        float curMultiTime = 0;

        [SerializeField] ObstacleGenerator generator;

        [Header("UI")]
        [SerializeField] GameObject DoneUI;
        [SerializeField] TextMeshProUGUI text_score;
        [SerializeField] TextMeshProUGUI text_ballAmount;

        [Space]
        [SerializeField] Image combo_fill;
        [SerializeField] TextMeshProUGUI comboAmount;

        private void Update()
        {
            if (multiple && curMultiTime > 0)
            {
                curMultiTime -= Time.deltaTime;
                combo_fill.fillAmount = curMultiTime / multipleDuration;
            }
            else
            {
                multiple = false;
                multipleAmount = 0;
                comboAmount.text = "X"+multipleAmount.ToString();
            }
        }

        public void StartGame()
        {
            StartCoroutine(GameReady());
        }
        public void UnStartGame()
        {
            isGameStart = false;
        }
        void EndGame()
        {
            if (obstacleAmount > 0)
            {
                DoneUI.SetActive(true);
                SQLControl.StoreGameResult(gameID,
                   UserData.scoreGame[gameID] < score ? score : UserData.scoreGame[gameID]);
            }
        }
        public void CallEndGame()
        {
            Invoke("EndGame", 7);
        }

        public void AddScore(int score)
        {
            if (multiple)
            {
                this.score += score * multipleAmount;
            }
            else
            {
                this.score += score;
            }

            multipleAmount++;
            multipleAmount = Mathf.Clamp(multipleAmount, 1, 5);
            comboAmount.text = "X"+multipleAmount.ToString();

            curMultiTime = multipleDuration;
            multiple = true;

            text_score.text = this.score.ToString();

        }

        public void ObstacleCreated()
        {
            obstacleAmount++;
        }
        public void ObstacleDestroyed()
        {
            obstacleAmount--;
            if (obstacleAmount <= 0)
            {
                CancelInvoke("EndGame");
                generator.GenerateAgain();
                shootAmount += 2;
            }
        }

        public void UpdateBallAmout()
        {
            text_ballAmount.text = shootAmount.ToString();
        }

        IEnumerator GameReady()
        {
            yield return new WaitForSeconds(1);
            isGameStart = true;
            UpdateBallAmout();
        }
        #region BTN Action

        public void ExitGame()
        {
            SceneLoader.UnloadScene(4, delegate
            {
                GlobalGameManager.ChangeGameState(GameState.vtour);
            });
        }
        public void Restart()
        {
            SceneLoader.ReloadScene(4);
        }

        #endregion
    }
}
