using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AngryBird
{
    public class ObstacleControl : MonoBehaviour
    {
        public Transform nextObstaclePos;

        [SerializeField] Rigidbody[] childrigid;

        private void OnEnable()
        {
            SleepRigid();

            Invoke("WakeRigid",.5f);
        }

        void SleepRigid()
        {
            foreach(var item in childrigid)
            {
                item.isKinematic = true;
            }
        }

        void WakeRigid()
        {
            foreach (var item in childrigid)
            {
                item.isKinematic = false;
            }
        }
    }
}
