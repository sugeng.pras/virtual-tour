using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VtourMinigames;
namespace SideScroll
{
    public class Platform : MonoBehaviour, IEndZoneReciver
    {
        PlatformControl controller;

        public void Initial(PlatformControl control)
        {
            controller = control;
        }

        public void ReachEndZone()
        {
            controller.StoreHidObject(this.gameObject);
        }
    }
}
