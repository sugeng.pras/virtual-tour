using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScroll
{
    public class PlatformControl : MonoBehaviour
    {
        List<GameObject> createdPlatform;
        [SerializeField] Platform platformSample;

        [SerializeField] Transform platformPlace;

        GameObject lastCreated;

        List<GameObject> hideObject = new List<GameObject>();

        private void Start()
        {
            createdPlatform = new List<GameObject>();
            GeneratePlatform();
        }

        public void GeneratePlatform()
        {
            for (int i = 0; i < 6; i++)
            {
                Platform curentPlatform = Instantiate(platformSample);
                if (createdPlatform.Count == 0)
                {
                    curentPlatform.transform.position = Vector3.zero;
                }
                else
                {
                    curentPlatform.transform.position = GetObjectNextPos(createdPlatform[i - 1], curentPlatform.gameObject);
                }
                curentPlatform.Initial(this);
                curentPlatform.transform.SetParent(platformPlace, true);

                lastCreated = curentPlatform.gameObject;
                createdPlatform.Add(curentPlatform.gameObject);

            }
        }
        public void StoreHidObject(GameObject target)
        {
            target.SetActive(false);
            hideObject.Add(target);

            if (hideObject.Count > 2)
            {
                SetNewPlatform();
            }
        }

        void SetNewPlatform()
        {
            if (hideObject.Count > 0)
            {
                hideObject[0].transform.position = GetObjectNextPos(lastCreated, hideObject[0]);

                lastCreated = hideObject[0];
                lastCreated.SetActive(true);

                hideObject.Remove(lastCreated);
            }
        }

        Vector3 GetObjectNextPos(GameObject targetBefore, GameObject target)
        {
            Bounds areaBefore = targetBefore.GetComponent<Renderer>().bounds;
            Bounds areaTarget = target.GetComponent<Renderer>().bounds;

            Vector3 nextPos = new Vector3(areaBefore.max.x, areaBefore.center.y, areaBefore.center.z) +
                new Vector3(areaTarget.extents.x, 0, 0);

            return nextPos;
        }
    }
}
