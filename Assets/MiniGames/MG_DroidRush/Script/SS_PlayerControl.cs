using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScroll
{
    public class SS_PlayerControl : MonoBehaviour
    {
        SS_GameManager gameControl;
        Rigidbody rigid;
        //UnitAnimationControl unitAnimator;
        VtourMinigames.UnitAnimatorControl animControl;

        [Header(" Hit Collider Setting")]
        [SerializeField] Vector3 oriCenter;
        [SerializeField] Vector3 oriSize;
        [Space]
        [SerializeField] Vector3 rolCenter;
        [SerializeField] Vector3 rolSize;

        [SerializeField] BoxCollider unitCollider;

        public bool isRunning { private set; get; }
        [SerializeField] bool canInput = true;

        [Space]
        [SerializeField] float runningSpeed;
        [SerializeField] float JumpPower;

        private void Start()
        {
            rigid = GetComponent<Rigidbody>();
            gameControl = FindObjectOfType<SS_GameManager>();
            // unitAnimator = GetComponent<UnitAnimationControl>();
            animControl = new VtourMinigames.UnitAnimatorControl(GetComponentInChildren<Animator>());
            Invoke("StartRunning", 1);
        }


        private void Update()
        {
            if (isRunning && !gameControl.isGameOver)
            {
                rigid.velocity = new Vector3(runningSpeed, rigid.velocity.y, rigid.velocity.z);
            }

            if (canInput)
            {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    StartJump();
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    StartRolling();
                }

            }
        }

        private void OnTriggerEnter(Collider collision)
        {
            if (collision.gameObject.tag == "obstacle")
            {
                isRunning = false;
                if (canInput) animControl.RequestAnim("Crash2");
                else animControl.RequestAnim("Crash");

                //unitAnimator.CallStandAnim();
                gameControl.GameOver();
            }
            if (collision.gameObject.tag == "platform")
            {
                EndJump();
            }
        }

        void StartJump()
        {
            rigid.AddForce(Vector3.up * JumpPower, ForceMode.Impulse);
            animControl.RequestAnim("Jump");
            canInput = false;
        }
        void EndJump()
        {
            if (isRunning) animControl.RequestAnim("Run");
            canInput = true;
        }

        void StartRolling()
        {
            animControl.RequestAnim("Slide");

            unitCollider.center = rolCenter;
            unitCollider.size = rolSize;

            canInput = false;
            StartCoroutine(EndRolling());

        }
        IEnumerator EndRolling()
        {
            yield return new WaitForSeconds(1f);
            canInput = true;
            unitCollider.center = oriCenter;
            unitCollider.size = oriSize;
            animControl.RequestAnim("Run");
        }

        void StartRunning()
        {
            animControl.RequestAnim("Run");
            gameControl.GameStart();
            isRunning = true;
            canInput = true;
        }
    }
}
