using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScroll
{
    public class SS_GameManager : VtourMinigames.BaseMiniGameManager
    {
        bool startCount;
        float runTime;

        public bool isGameOver;
        protected override int GameID() => 0;

        private void Update()
        {
            if (isGameStart)
            {
                runTime += Time.deltaTime * 10;
                curentScore = Mathf.FloorToInt(runTime);

                text_curentScore.text = curentScore.ToString();
            }
        }
    }
}
