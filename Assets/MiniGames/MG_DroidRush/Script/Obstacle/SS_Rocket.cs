using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScroll
{

    public class SS_Rocket : SS_Obstacle
    {
        public float speed;
        private void Update()
        {
            if (gameObject.activeSelf)
                transform.Translate(Vector3.left * speed *Time.deltaTime);
        }
    }
}
