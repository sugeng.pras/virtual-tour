using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VtourMinigames;

namespace SideScroll
{
    public class SS_ObstacleGenerator : CuntinousObstacleGenerator
    {
        List<GameObject> hideUpper = new List<GameObject>();
        List<GameObject> hideBottom = new List<GameObject>();
        List<GameObject> hideRocket = new List<GameObject>();

        protected override void CreateObstacle()
        {
            GetObstaclePos(out int index);

            if (createdObstacle.Count <= poolObjectLimit)
            {
                SS_Obstacle curent = Instantiate((SS_Obstacle)obstaclePerf[index]);
                curent.transform.position = summonPlaces[index].position;
                curent.transform.localScale = summonPlaces[index].localScale;

                curent.InitialData(this, index);
                curent.transform.SetParent(obstaclePlace, true);
            }
            else
            {
                SetNewPlace(index);
            }

            summonRateTime = Mathf.Clamp(summonRateTime - 0.1f, 1.5f, 3);
        }
        void SetNewPlace(int index)
        {
            GameObject target;
            if (index == 1)
            {
                target = hideBottom[0];
                hideBottom.Remove(target);
            }
            else if (index == 0)
            {
                target = hideUpper[0];
                hideUpper.Remove(target);
            }
            else
            {
                target = hideRocket[0];
                hideRocket.Remove(target);
            }

            target.transform.position = summonPlaces[index].position;
            target.SetActive(true);
        }

        public void StoreObstacle(int index, GameObject target)
        {
            if (index == 1)
            {
                hideUpper.Add(target);
            }
            else if(index ==0)
            {
                hideBottom.Add(target);
            }
            else
            {
                hideRocket.Add(target);
            }
        }

        Vector3 GetObstaclePos(out int index)
        {
            int placeIndex = Random.Range(0, summonPlaces.Length);
            index = placeIndex;

            return new Vector3(
                summonPlaces[placeIndex].position.x,
                summonPlaces[placeIndex].position.y,
                0);
        }
    }
}
