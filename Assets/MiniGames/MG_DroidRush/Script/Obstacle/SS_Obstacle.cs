using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VtourMinigames;

namespace SideScroll
{
    public class SS_Obstacle : BaseObstacle
    {
        SS_ObstacleGenerator manager;
        int obstacleIndex;

        [SerializeField] Sprite[] variantSprites;
        [SerializeField] SpriteRenderer renderSprite;

        private void OnEnable()
        {
            renderSprite.sprite = variantSprites[Random.Range(0, variantSprites.Length)];
        }

        public override void ReachEndZone()
        {
            gameObject.SetActive(false);
            manager.StoreObstacle(obstacleIndex, this.gameObject);
        }

        public void InitialData(SS_ObstacleGenerator manager, int index)
        {
            this.manager = manager;
            obstacleIndex = index;
        }
    }
}
