using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VtourMinigames;

namespace ClimbRush
{
    public class CR_MapGenerator : MonoBehaviour
    {
        [SerializeField] BaseMiniGameManager gameManager;
        [SerializeField] Transform basePositionSummon;
        [SerializeField] Transform createdObjectContainer;
        [SerializeField] CR_MapObstacle mapPerfabs;
        //[SerializeField] MapObjectContainer[] mapContainer;
        List<GameObject> createdObject = new List<GameObject>();

        int curentBaseIndex;


        GameObject lastCreatedObject;

        private void Awake()
        {
            for (int i = 0; i < 3; i++)
            {
                CreateMap();
            }
        }

        void CreateMap()
        {
            CR_MapObstacle curent = Instantiate(mapPerfabs);

            if (lastCreatedObject == null)
            {
                curent.transform.position = Vector3.zero;
            }
            else
            {
                curent.transform.position = GetObjectNextPos(lastCreatedObject.gameObject, curent.gameObject);
            }

            curent.transform.SetParent(createdObjectContainer);
            lastCreatedObject = curent.gameObject;
            //mapContainer[baseIndex].createdMap.Add(curent);
            curent.Initial(-1, this);
        }

        //void CreateMap(int baseIndex)
        //{
        //    MapObjectContainer temp_container = mapContainer[baseIndex];

        //    if (temp_container.createdMap.Count <= 3)
        //    {
        //        int index = temp_container.createdMap.Count;

        //        CR_MapObstacle curent = Instantiate(temp_container.mapBaseObject[index]);

        //        if (lastCreatedObject == null)
        //        {
        //            curent.transform.position = Vector3.zero;
        //        }
        //        else
        //        {
        //            curent.transform.position = GetObjectNextPos(lastCreatedObject.gameObject, curent.gameObject);
        //        }

        //        lastCreatedObject = curent.gameObject;
        //        mapContainer[baseIndex].createdMap.Add(curent);
        //        curent.Initial(baseIndex, this);
        //    }
        //}

        void ReSetMap(int baseIndex)
        {
            //int random = Random.Range(0, mapContainer[baseIndex].hidenObject.Count);

            //GameObject selected = mapContainer[baseIndex].hidenObject[random];
            GameObject selected = createdObject[0];
            selected.transform.position = GetObjectNextPos(lastCreatedObject.gameObject, selected);

            selected.SetActive(true);
            lastCreatedObject = selected;
            createdObject.Remove(selected);
            //mapContainer[baseIndex].hidenObject.Remove(selected);

        }
        public void StoreHiddenObject(int baseIndex, GameObject target)
        {
            //mapContainer[baseIndex].hidenObject.Add(target);
            createdObject.Add(target);
            target.SetActive(false);
            ReSetMap(baseIndex);
        }

        Vector3 GetObjectNextPos(GameObject targetBefore, GameObject target)
        {
            Bounds areaBefore = targetBefore.GetComponent<Renderer>().bounds;
            Bounds areaTarget = target.GetComponent<Renderer>().bounds;

            Vector3 nextPos = new Vector3(basePositionSummon.position.x, areaBefore.max.y, basePositionSummon.position.z) +
                new Vector3(0, areaTarget.extents.y, 0);

            return nextPos;
        }


        [System.Serializable]
        public class MapObjectContainer
        {
            public CR_MapObstacle[] mapBaseObject;
            public List<BaseObstacle> createdMap;
            public List<GameObject> hidenObject;
        }
    }
}
