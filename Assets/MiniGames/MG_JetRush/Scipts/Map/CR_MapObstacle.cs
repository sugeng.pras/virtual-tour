using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ClimbRush
{
    public class CR_MapObstacle : VtourMinigames.BaseObstacle
    {
        CR_MapGenerator generatorControl;
        [SerializeField] int objectBaseIndex;
        public void Initial(int baseIndex, CR_MapGenerator generatorControl)
        {
            this.generatorControl = generatorControl;
            objectBaseIndex = baseIndex;
        }

        public override void ReachEndZone()
        {
            Debug.Log("Here");
            generatorControl.StoreHiddenObject(objectBaseIndex, this.gameObject);
        }
    }
}
