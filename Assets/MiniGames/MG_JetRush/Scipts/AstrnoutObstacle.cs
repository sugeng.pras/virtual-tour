using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ClimbRush
{
    public class AstrnoutObstacle :VtourMinigames.BaseObstacle
    {
        private void OnTriggerEnter(Collider other)
        {
            if(other.tag == "Player")
            {
                controller.StoreHidenObject(this.gameObject);
                this.gameObject.SetActive(false);
            }
        }
    }
}
