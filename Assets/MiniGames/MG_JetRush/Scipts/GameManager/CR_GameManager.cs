using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ClimbRush
{
    public class CR_GameManager : VtourMinigames.BaseMiniGameManager
    {
        private void Awake()
        {
            GameStart();
        }
        protected override int GameID() => 2;
        public void AddScore(int amount)
        {
            curentScore += amount;
            text_curentScore.text = curentScore.ToString();
        }
        
        public override void GameOver()
        {
            CancelInvoke("AddSocreEachSecond");
            base.GameOver();
        }

        public override void GameStart()
        {
            base.GameStart();

            InvokeRepeating("AddSocreEachSecond", 0, 1);
        }

        void AddSocreEachSecond()
        {
            AddScore(10);

        }
    }
}
