using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ClimbRush
{


    public class CR_HitObstacle : VtourMinigames.BaseObstacle
    {
        public float rotateSpeed;

        // Update is called once per frame
        void Update()
        {
            transform.Rotate(0, 0, rotateSpeed);
        }

        public override void ReachEndZone()
        {
            controller.StoreHidenObject(gameObject);
            gameObject.SetActive(false);
        }

        public void StoreObject()
        {
            controller.StoreHidenObject(gameObject);
            gameObject.SetActive(false);
        }
    }
}
