using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClimbRush
{
    public class CR_ClimbControl : MonoBehaviour
    {
        [SerializeField] float climbSpeed;

        private void Start()
        {
            InvokeRepeating("IncreaseClimbspeed", 1, 4f);
        }

        private void Update()
        {
            transform.Translate(Vector3.up * Time.deltaTime * climbSpeed);
        }

        private void IncreaseClimbspeed()
        {
            climbSpeed = Mathf.Clamp(climbSpeed + 0.2f, 2, 3);
        }
    }
}
