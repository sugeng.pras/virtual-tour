using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ClimbRush
{
    public interface ICRInputReciver
    {
        void VerticalMove(float inputValue);

        void HorizontalMove(float inputValue);

    }
}
