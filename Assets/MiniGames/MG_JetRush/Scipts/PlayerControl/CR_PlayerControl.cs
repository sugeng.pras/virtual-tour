using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ClimbRush
{
    public class CR_PlayerControl : MonoBehaviour, ICRInputReciver
    {
        [SerializeField] BoxCollider area;
        [SerializeField] BoxCollider player;
        [SerializeField] CR_GameManager manager;

        [SerializeField] int playerHealth;
        [SerializeField] int curentPlayerHealth;

        Vector2 playerSize;

        [SerializeField] float moveSpeed = 2;

        private void Awake()
        {
            curentPlayerHealth = playerHealth;
            playerSize = new Vector2(
                (player.bounds.size.x / 2) + 0.2f,
                (player.bounds.size.y / 2));
        }

        private void Update()
        {
            Vector2 curPos = transform.position;
            Vector2 fixPos = new Vector2(
                Mathf.Clamp(curPos.x, area.bounds.min.x + playerSize.x, area.bounds.max.x - playerSize.x),
                  Mathf.Clamp(curPos.y, area.bounds.min.y + playerSize.y, area.bounds.max.y - playerSize.y)
                 );

            transform.position = fixPos;

        }

        public void HorizontalMove(float inputValue)
        {
            if (inputValue > 0)
            {
                MoveRight();
            }
            else if (inputValue < 0)
            {
                MoveLeft();
            }
        }

        public void VerticalMove(float inputValue)
        {

            if (inputValue > 0)
            {
                MoveUp();
            }
            else if (inputValue < 0)
            {
                MoveDown();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "obstacle")
            {
                curentPlayerHealth -= 10;
                if (curentPlayerHealth <= 0)
                {
                    Debug.Log(other.gameObject.name);
                    manager.GameOver();
                }
            }
            else if (other.tag == "powerUp")
            {
                manager.AddScore(50);
                other.GetComponent<CR_HitObstacle>().StoreObject();
            }
        }

        void MoveUp()
        {
            transform.localPosition = new Vector3(

                transform.localPosition.x, transform.localPosition.y + Time.deltaTime * moveSpeed, -3
            );
        }
        void MoveDown()
        {
            transform.localPosition = new Vector3(

                transform.localPosition.x, transform.localPosition.y - Time.deltaTime * moveSpeed, -3
            );
        }

        void MoveRight()
        {
            transform.localPosition = new Vector3(

                transform.localPosition.x + Time.deltaTime * moveSpeed, transform.localPosition.y, -3
            );
        }
        void MoveLeft()
        {
            transform.localPosition = new Vector3(

                transform.localPosition.x - Time.deltaTime * moveSpeed, transform.localPosition.y, -3
            );
        }

    }


}
