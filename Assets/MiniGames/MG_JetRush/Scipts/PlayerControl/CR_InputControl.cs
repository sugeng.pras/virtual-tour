using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClimbRush
{
    public class CR_InputControl : MonoBehaviour
    {
        [SerializeField] ICRInputReciver player;
        [SerializeField] GameObject playerObject;

        private void Awake()
        {
            player = playerObject.GetComponent<ICRInputReciver>();
        }

        // Update is called once per frame
        void Update()
        {
            player.HorizontalMove(Input.GetAxisRaw("Horizontal"));
            player.VerticalMove(Input.GetAxisRaw("Vertical"));
        }
    }

}