using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ClimbRush
{
    public class CR_ObstacleGenerator : VtourMinigames.CuntinousObstacleGenerator
    {
        public BoxCollider areaSummon;
        Vector2 minPos, maxPos;

        [SerializeField] float minTime, maxTime;

        private void Awake()
        {
            InvokeRepeating("IncreaseSummonRate", 0, 5);
        }

        protected override Vector3 GetSummonPlace()
        {
            minPos = new Vector3(areaSummon.bounds.min.x, areaSummon.bounds.min.y);
            maxPos = new Vector3(areaSummon.bounds.max.x, areaSummon.bounds.max.y);

            return new Vector3
                (Random.Range(minPos.x, maxPos.x),
                Random.Range(minPos.y, maxPos.y), 0
                );
        }


        void IncreaseSummonRate()
        {
            summonRateTime = Mathf.Clamp(summonRateTime - 0.05f, minTime, maxTime);
        }
    }
}
