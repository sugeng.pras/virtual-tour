using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class SQLControl : MonoBehaviour
{
    [SerializeField] PhotonLobby lobbyControl;
    [SerializeField] UIControl_Lobby lobby_ui;

    const string baseUrl = "https://satriverstudio.com/moderator2/testPhp/";

    public static SQLControl _instance;

    const string createUserData = baseUrl + "storeUserData.php";
    const string login = baseUrl + "login.php";
    const string logout = baseUrl + "logout.php";

    const string enterGame = baseUrl + "enterGame.php";

    const string storeGameScore = baseUrl + "storeGameScore.php";
    const string getAllUserScore = baseUrl + "getAllUserScore.php";

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    #region DB Field Name
    const string userIDField = "userID";
    const string usernameField = "name";
    const string passField = "password";

    const string emailField = "email";
    const string companyField = "company";
    const string positionField = "position";

    const string comeTimeField = "cometime";

    readonly string[] gameScoreField = new string[] {
        "score1", "score2", "score3", "score4" };

    #endregion

    #region Create UserData (SignUP)
    public static void CreateUserData(string userName, string email, string company, string position)
    {
        _instance.StartCoroutine(_instance.CreateToBase(userName, email, company, position));
    }
    IEnumerator CreateToBase(string userName, string email, string company, string position)
    {
        lobby_ui.signUp.interactable = false;
        WWWForm form = new WWWForm();

        form.AddField(usernameField, userName);
        form.AddField(emailField, email);
        form.AddField(companyField, company);
        form.AddField(positionField, position);

        using (UnityWebRequest request = UnityWebRequest.Post(createUserData, form))
        {
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(request.error);

                lobby_ui.signUp.interactable = true;
                lobby_ui.SendNotif(request.error, Color.red);
            }
            else
            {
                string requestResult = request.downloadHandler.text;
                if (requestResult == "0")
                {
                    lobby_ui.signUp.interactable = true;
                    lobby_ui.SendNotif("Success Create User", Color.green);

                    Login(userName, null);
                }
                else
                {
                    lobby_ui.SendNotif(requestResult, Color.red);
                    lobby_ui.signUp.interactable = true;
                }
            }
        }
    }
    #endregion

    #region Login
    public static void Login(string username, System.Action onLoginEvent)
    {
        _instance.StartCoroutine(_instance.CheckLoginToBase(username, -1, onLoginEvent));
    }

    IEnumerator CheckLoginToBase(string username, int userID, System.Action onLoginEvent)
    {
        WWWForm form = new WWWForm();
        // Checking Input
        if (username != null)
        {
            form.AddField(usernameField, username);
        }
        else if (userID != -1)
        {
            form.AddField(userIDField, userID);
        }
        else
        {
            Debug.Log("Fail Get send Data to server");
            yield return null;
        }

        //Action on PHP
        using (UnityWebRequest request = UnityWebRequest.Post(login, form))
        {
            lobby_ui.SendNotif("Check Account...", Color.black);
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(request.error);
                lobby_ui.joinBtn.interactable = true;
            }
            else
            {
                string requestResult = request.downloadHandler.text;
                string[] varaiable = requestResult.Split('/'); //[0]: userID; [1]: username; [3]: Scoregame1; [4]: Scoregame2; [5]: Scoregame3; [6]: Scoregame4

                if (varaiable.Length > 1)
                {
                    UserData.userID = int.Parse(varaiable[0]);
                    UserData.userName = varaiable[1];

                    for (int i = 2; i < 6; i++)
                    {
                        UserData.scoreGame[i - 2] = int.Parse(varaiable[i]);
                    }

                    Debug.Log("your Game score has sync: " + UserData.userName);

                    if (lobbyControl != null) lobbyControl.ConectToServer(UserData.userName);

                    if (onLoginEvent != null) onLoginEvent();
                }
                else
                {
                    if (lobby_ui != null) lobby_ui.SendNotif(requestResult, Color.red);
                    lobby_ui.joinBtn.interactable = true;
                }

            }
        }
    }
    #endregion

    #region Join A room (Join Game)
    public static void StoreJoinHistory()
    {
        _instance.StartCoroutine(_instance.StoreHistory());
    }
    IEnumerator StoreHistory()
    {
        WWWForm form = new WWWForm();

        form.AddField(userIDField, UserData.userID);
        form.AddField(usernameField, UserData.userName);
        form.AddField(comeTimeField, System.DateTime.UtcNow.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss"));

        using (UnityWebRequest request = UnityWebRequest.Post(enterGame, form))
        {
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(request.error);
            }
            else
            {
                string requestResult = request.downloadHandler.text;

                //Debug.Log(requestResult);
            }
        }
    }
    #endregion

    #region Store Game result
    public static void StoreGameResult(int gameID, int score)
    {
        _instance.StartCoroutine(_instance.StoreGameScore(gameID, score));
    }
    IEnumerator StoreGameScore(int gameID, int score)
    {
        WWWForm form = new WWWForm();

        form.AddField(usernameField, UserData.userName);
        form.AddField("scoreRow", gameScoreField[gameID]);
        form.AddField(gameScoreField[gameID], score);

        using (UnityWebRequest request = UnityWebRequest.Post(storeGameScore, form))
        {
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(request.error);
            }
            else
            {
                string requestResult = request.downloadHandler.text;

                Debug.Log(requestResult);
            }
        }
    }
    #endregion

    #region Get All User Game Score
    public static void SetScoreBoardData()
    {
        _instance.StartCoroutine(_instance.SetAllUserScore());
    }
    IEnumerator SetAllUserScore()
    {
        WWWForm form = new WWWForm();

        //Action on PHP
        using (UnityWebRequest request = UnityWebRequest.Post(getAllUserScore, form))
        {
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(request.error);
            }
            else
            {
                string requestResult = request.downloadHandler.text;
                string[] userData = requestResult.Split(','); //[0]: username;  [1]: Scoregame1; [2]: Scoregame2; [3]: Scoregame3; [4]: Scoregame4

                if (userData.Length > 0)
                {
                    FindObjectOfType<LeaderBoardUI>().SetScoreValue(userData);

                }
                else
                {
                    if (lobby_ui != null) lobby_ui.SendNotif(requestResult, Color.red);
                }

            }
        }
    }
    #endregion

    #region Logout
    public static void UserLogout(string userName)
    {
        _instance.StartCoroutine(_instance.Logout(userName));
    }

    IEnumerator Logout(string userName)
    {
        WWWForm form = new WWWForm();
        form.AddField(usernameField, userName);
        using (UnityWebRequest request = UnityWebRequest.Post(logout, form))
        {
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(request.error);
            }
            else
            {
                string requestResult = request.downloadHandler.text;

                Debug.Log(requestResult);
            }
        }
    }
    #endregion
}
