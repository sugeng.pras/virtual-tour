using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[System.Serializable]
public class CameraManager
{
    [SerializeField] CinemachineVirtualCamera v_mainCam;
    [SerializeField] CinemachineBrain b_mainCamera;
    [SerializeField] Camera mainCam;
    [Space]
    [SerializeField] CinemachineVirtualCamera v_photoCamera;
    [SerializeField] CinemachineBrain b_photoCamera;
    [SerializeField] Camera photoCam;

    public void EnableCamera_main(bool isActive)
    {
        mainCam.enabled = isActive;
        v_mainCam.enabled = isActive;
        b_mainCamera.enabled = isActive;
    }
    public void EnableCamera_photo(bool isActive)
    {
        photoCam.enabled = isActive;
        v_photoCamera.enabled = isActive;
        b_photoCamera.enabled = isActive;
    }

}
