using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class AvatarManager : MonoBehaviour
{
    int curHeadCode;
    int curBodyCode;
    int curTorsoCode;
    int curHairCode;

    [SerializeField] GameObject playerAvatarView;
    Hashtable _custProp = new Hashtable();

    #region Avatar Data
    [SerializeField] Material[] headParts;
    [SerializeField] Material[] bodyParts;
    [SerializeField] Material[] torsoParts;

    #endregion

    #region Component of Avatar
    SkinnedMeshRenderer headAva;
    SkinnedMeshRenderer bodyAva;
    SkinnedMeshRenderer torsoAva;
    [SerializeField] GameObject[] hairAva;

    void GetAvatarComponent(GameObject avatarObject, out SkinnedMeshRenderer headAva, out SkinnedMeshRenderer bodyAva, out SkinnedMeshRenderer torsoAva, out GameObject[] hairObjects)
    {
        headAva = avatarObject.transform.Find("Kepala").GetComponent<SkinnedMeshRenderer>();
        bodyAva = avatarObject.transform.Find("Badan").GetComponent<SkinnedMeshRenderer>();
        torsoAva = avatarObject.transform.Find("Kaki").GetComponent<SkinnedMeshRenderer>();
        hairObjects = new GameObject[9];

        for (int i = 0; i < 9; i++)
        {
            hairObjects[i] = avatarObject.transform.Find("root/root.x/spine_01.x/spine_02.x/neck.x/head.x/"+i.ToString()).gameObject;
            hairAva[i].SetActive(false);
        }
    }

    #endregion

    private void Awake()
    {
        if (GameControl_Vtour._instance != null)
        {
            GameControl_Vtour._instance.OnMainGameStart += delegate
            {
                EnableAvatarView(false);
            };
            GameControl_Vtour._instance.OnMainGameOver += delegate
            {
                EnableAvatarView(true);
            };
        }
    }
    private void Start()
    {
        if (playerAvatarView != null)
        {
            GetAvatarComponent(playerAvatarView,
                out this.headAva,
                out this.bodyAva,
                out this.torsoAva,
                out this.hairAva
                );
        }
    }

    public void SetHair(bool isNext)
    {
        int beforecode = curHairCode;

        if (isNext) curHairCode++;
        else curHairCode--;
        curHairCode %= hairAva.Length;

        hairAva[beforecode].SetActive(false);
        hairAva[curHairCode].SetActive(true);
    }
    public void SetHead(bool isNext)
    {
        if (isNext) curHeadCode++;
        else curHeadCode--;
        int beforecode = curHeadCode;
        curHeadCode %= headParts.Length;

        headAva.material = headParts[Mathf.Abs(curHeadCode)];

        //SetHair(gender, beforecode, curHeadCode);
    }
    public void SetBody(bool isNext)
    {
        if (isNext) curBodyCode++;
        else curBodyCode--;

        curBodyCode %= bodyParts.Length;
        bodyAva.material = bodyParts[Mathf.Abs(curBodyCode)];
    }
    public void SetTorso(bool isNext)
    {
        if (isNext) curTorsoCode++;
        else curTorsoCode--;

        curTorsoCode %= torsoParts.Length;
        torsoAva.material = torsoParts[Mathf.Abs(curTorsoCode)];
    }




    public void ApplyAvatar(GameObject targetAvatarObject, int[] data)
    {
        GetAvatarComponent(targetAvatarObject,
            out SkinnedMeshRenderer headAva, out SkinnedMeshRenderer bodyAva, out SkinnedMeshRenderer torsoAva, out GameObject[] hair);


        hair[data[0]].SetActive(true); // rambut
        headAva.material = headParts[data[1]]; // wajah
        bodyAva.material = bodyParts[data[2]]; //Badan
        torsoAva.material = torsoParts[data[3]];//kaki



    }
    public int[] GetAvatarCode()
    {
        return new int[]
        {
            Mathf.Abs(curHairCode), //rambut
            Mathf.Abs(curHeadCode), //wajah
            Mathf.Abs(curBodyCode), //tubuh
            Mathf.Abs(curTorsoCode), // kaki
        };
    }
    public void SaveAvatarCode(int[] avatarCode)
    {
        _custProp["avatarCode"] = avatarCode;
        PhotonNetwork.SetPlayerCustomProperties(_custProp);
        UserData.avatarCode = avatarCode;
    }

    void EnableAvatarView(bool isActive)
    {
        playerAvatarView.transform.parent.gameObject.SetActive(isActive);
    }

}
