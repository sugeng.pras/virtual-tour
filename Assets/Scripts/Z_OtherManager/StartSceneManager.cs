using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class StartSceneManager : MonoBehaviour
{
    [SerializeField] int firstScene;
    private void Awake()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(firstScene, UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }
    public void OpenMainGame(int sceneTarget)
    {
        SceneLoader.RequestLoadScene(sceneTarget);
    }
}
