using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalGameManager : MonoBehaviour
{
    static GlobalGameManager _instance;

    [Header("Hide Vtour Feature")]

    [SerializeField] GameObject world;
    [SerializeField] GameObject cam, HUD;


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }
    public static GameState curentGameState { private set; get; }

    public static void ChangeGameState(GameState state)
    {
        bool check = (state == GameState.vtour);

        _instance.world.SetActive(check);
        _instance.cam.SetActive(check);
        _instance.HUD.SetActive(check);

        curentGameState = state;
    }
    
}

public enum GameState
{
    none,
    vtour,
    miniGame
}
