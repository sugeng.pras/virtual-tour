using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;


public class InterestAreaManager : MonoBehaviour
{
    Hashtable _custProp = new Hashtable();
    [SerializeField] List<InterestArea_Network> interestAreas;
    [SerializeField] PhotonRoomSetting roomSetting;


    public void SetInterestArea(PhotonView client, InterestArea_Network area)
    {
        int areaIndex = interestAreas.FindIndex(x => x == area) + 1;

        _custProp["interestArea"] = (byte)areaIndex;
        client.Owner.SetCustomProperties(_custProp);
        client.Group = (byte)areaIndex;

        PhotonNetwork.SetInterestGroups((byte)areaIndex, true);

        _custProp["interestArea"] = (byte)areaIndex;
        client.Owner.SetCustomProperties(_custProp);
    }

    public void LeaveInterestArea(PhotonView client, InterestArea_Network area)
    {

        int areaIndex = interestAreas.FindIndex(x => x == area) + 1;

        client.Group = 0;
        //_custProp["interestArea"] = (byte)0;
        //client.Owner.SetCustomProperties(_custProp);

        PhotonNetwork.SetInterestGroups((byte)areaIndex, false);

    }

}
