using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterestArea_Client : MonoBehaviour
{
    [SerializeField] int limitClientOnView;
    [SerializeField] int curentClientOnView;
    [SerializeField] PhotonView ownPv;
    [SerializeField] byte interestArea;

    private void Awake()
    {
        this.gameObject.SetActive(ownPv.IsMine);
        //transform.parent.Find("Avatar").gameObject.SetActive(ownPv.IsMine);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PhotonView pv = other.gameObject.GetPhotonView();
            if ((byte)pv.Owner.CustomProperties["interestArea"] == (byte)ownPv.Owner.CustomProperties["interestArea"])
            {
                Debug.Log("Same Area");
                other.transform.Find("Avatar").gameObject.SetActive(true);
                curentClientOnView++;
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            PhotonView pv = other.gameObject.GetPhotonView();
            GameObject otherPlayer = other.transform.Find("Avatar").gameObject;
            if (otherPlayer.activeSelf)
            {
                curentClientOnView--;
                otherPlayer.SetActive(false);
            }

        }
    }
}
