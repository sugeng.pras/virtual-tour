using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class InterestArea_Network : MonoBehaviour
{
    [SerializeField] InterestAreaManager manager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PhotonView pv = other.gameObject.GetPhotonView();
            if (pv.IsMine)
            {
                manager.SetInterestArea(pv, this);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            PhotonView pv = other.gameObject.GetPhotonView();
            if (pv.IsMine)
            {
                pv.RPC("SetAvatarRender", RpcTarget.AllBuffered, false);
                manager.LeaveInterestArea(pv, this);
            }
        }
    }
}
