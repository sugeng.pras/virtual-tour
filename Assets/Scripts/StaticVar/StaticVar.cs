
public class StaticVar { }

public static class UserData
{
    public static int userID;
    public static string userName;

    public static int[] scoreGame =new int[4];

    public static int[] avatarCode;

    
    public static void ClearCurrentData()
    {
        userID = 0;
        userName = null;

        scoreGame = new int[4];
    }
}