using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KeyboadInputControl : MonoBehaviour
{
    Vector3 rayOutPos;
    [SerializeField] Vector2 outOfset;

    INavDestReciver player;


    [SerializeField] Camera usedCam;
    [SerializeField] float castRepeatTime;
    float curCastTime;

    bool isInputPress;

    private void Awake()
    {
        usedCam = Camera.main;
        player = GetComponent<INavDestReciver>();
    }

    // Update is called once per frame
    void Update()
    {
        SetOutPos();
        player.SetUnitMove(isInputPress);
    }
    private void FixedUpdate()
    {
        if (GetComponent<Photon.Pun.PhotonView>().IsMine)
        {
            if (isInputPress)
            {
                if (curCastTime <= 0)
                {
                    curCastTime = castRepeatTime;
                    GetNavPos();
                }
                else
                {
                    curCastTime -= Time.fixedDeltaTime;
                }
            }
            else
            {
                curCastTime = 0;
            }
        }
    }

    void SetOutPos()
    {
        float xPos = Input.GetAxisRaw("Horizontal");
        float zPos = Input.GetAxisRaw("Vertical");

        rayOutPos = new Vector3(xPos * outOfset.x, 1, zPos * outOfset.y);

        if (xPos != 0 || zPos != 0)
        {
            isInputPress = true;
        }
        else
        {
            isInputPress = false;
        }

    }

    Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        var dir = point - pivot; // get point direction relative to pivot
        dir = Quaternion.Euler(angles) * dir; // rotate it
        point = dir + pivot; // calculate rotated point
        return point; // return it
    }

    void GetNavPos()
    {
        if (player.unitTransform.GetComponent<UnitMovement>().canMove)
        {
            player.unitTransform.LookAt(RotatePointAroundPivot(rayOutPos + player.unitTransform.position,
            player.unitTransform.position,
            new Vector3(0, usedCam.transform.eulerAngles.y, 0)));
        }

        //Physics.Raycast(RotatePointAroundPivot(rayOutPos + player.unitTransform.position,
        //player.unitTransform.position,
        //new Vector3(0, usedCam.transform.eulerAngles.y, 0)),

        //Vector3.down, out RaycastHit hit, Mathf.Infinity);


        //NavMesh.SamplePosition(hit.point, out NavMeshHit hitNav, 1, NavMesh.AllAreas);

        //player.SetNavLocation(hitNav.position);
    }
}
