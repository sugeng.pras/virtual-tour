using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INavKeyboardReciver
{
    Transform unitTransform { get; }
    void SetKeyboardNavLocation(Vector3 navLocation);

    void OnHorizontalEvent(float dir);
    void OnVerticalEvent(float dir);
}
