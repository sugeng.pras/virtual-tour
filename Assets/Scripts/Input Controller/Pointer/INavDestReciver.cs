using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INavDestReciver
{
    Transform unitTransform {get; }
    void SetNavLocation (Vector3 navLocation);

    void SetUnitMove(bool isUnitMove);
}
