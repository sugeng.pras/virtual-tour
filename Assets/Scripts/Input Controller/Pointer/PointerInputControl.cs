using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class PointerInputControl : MonoBehaviour
{
    INavDestReciver player;
    Camera mainCam;

    private void Awake()
    {
        mainCam = Camera.main;
        player = GetComponent<INavDestReciver>();
    }
    private void Update()
    {
        if (!Input.GetMouseButton(0) || EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        Ray rayPoint = mainCam.ScreenPointToRay(Input.mousePosition);
        GetPositiOnOnMesh(rayPoint);
    }

    void GetPositiOnOnMesh(Ray rayPoint)
    {
        Physics.Raycast(rayPoint, out RaycastHit hit, Mathf.Infinity);
        NavMesh.SamplePosition(hit.point, out NavMeshHit hitNav, 1, NavMesh.AllAreas);

        player.SetNavLocation(hitNav.position);
    }
}

