using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] GameObject loadingScreen;
    [SerializeField] GameObject textLoading;
    [SerializeField] GameObject blockerScreen;

    static SceneLoader _instance;


    private void Update()
    {
        if (loadingScreen.activeSelf)
        {
            textLoading.transform.Rotate(new Vector3(1, 0, 0));
        }
    }
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }

        SceneManager.sceneLoaded += OnSceneLoaded;
        DontDestroyOnLoad(this);

    }
    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    public static void RequestLoadScene(int sceneTarget)
    {
        Scene target = SceneManager.GetSceneByBuildIndex(sceneTarget);
        if (!target.isLoaded)
        {
            _instance.loadingScreen.SetActive(true);
            SceneManager.LoadScene(sceneTarget);
        }
    }
    public static void RequestLoadScene(int sceneTarget, LoadSceneMode mode)
    {
        Scene target = SceneManager.GetSceneByBuildIndex(sceneTarget);
        if (!target.isLoaded)
        {
            _instance.loadingScreen.SetActive(true);
            SceneManager.LoadScene(sceneTarget, mode);
        }
    }
    public static void ReloadScene(int sceneTarget)
    {
        Scene target = SceneManager.GetSceneByBuildIndex(sceneTarget);
        if (target.isLoaded)
        {
            _instance.loadingScreen.SetActive(true);
            _instance.StartCoroutine(_instance.IE_ReloadScene(sceneTarget));
            SceneManager.UnloadSceneAsync(sceneTarget);
        }
    }
    public static void UnloadScene(int sceneTarget, System.Action OnUnloaded)
    {
        _instance.loadingScreen.SetActive(true);
        _instance.StartCoroutine(_instance.IE_UnloadScene(sceneTarget, OnUnloaded));
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        loadingScreen.SetActive(false);
    }

    IEnumerator IE_ReloadScene(int sceneIndex)
    {
        yield return SceneManager.UnloadSceneAsync(sceneIndex);

        RequestLoadScene(sceneIndex, LoadSceneMode.Additive);
    }
    IEnumerator IE_UnloadScene(int sceneIndex, System.Action OnDoneUnload)
    {
        yield return SceneManager.UnloadSceneAsync(sceneIndex);

        if (OnDoneUnload != null) OnDoneUnload();

        loadingScreen.SetActive(false);
    }
}
