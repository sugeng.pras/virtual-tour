using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class PhotonLobby : MonoBehaviourPunCallbacks
{

    [SerializeField] UIMainManager mainUI;
    [SerializeField] PhotonChatting chattingServer;
    [SerializeField] PhotonRoomSetting customRoomSetting;
    [SerializeField] Transform playerCreatedPlace;

    RoomOptions roomSetting;

    private void Start()
    {

        roomSetting = new RoomOptions()
        {
            IsOpen = customRoomSetting.IsOpen,
            IsVisible = customRoomSetting.IsVisible,
            PublishUserId = customRoomSetting.PublishUserId,
            MaxPlayers = customRoomSetting.MaxPlayer,

        };
    }

    bool isConnecting;

    #region PUNBehaviour

    public override void OnConnectedToMaster()
    {
        if (isConnecting)
        {
            PhotonNetwork.JoinRandomRoom();
        }
    }
    public override void OnJoinedRoom()
    {
        mainUI.uiLobby.SendNotif("",Color.black);

        GameObject baseObject = PhotonNetwork.Instantiate("SamplePerfabs",
         GameControl_Vtour.GetRandomPos(), Quaternion.identity);

        baseObject.transform.SetParent(playerCreatedPlace, false);

        chattingServer.ConnectToChatServer(PhotonNetwork.CurrentRoom.Name);

        mainUI.EnableUI_Lobby(false);
        mainUI.EnableUI_Avatar(false);

        mainUI.EnableUI_InGame(true);

        mainUI.uiLobby.ClearForm();

        GameControl_Vtour._instance.CallOnMainGameStart();

        SQLControl.StoreJoinHistory();

        print("You Join A room");

        GlobalGameManager.ChangeGameState(GameState.vtour);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        print("return Code : " + returnCode + "\n Message : " + message);
        if (PhotonNetwork.CountOfRooms < customRoomSetting.MaxCreatedRoom) CreateRoom();
        else
        {
            print("Limit Room");
            mainUI.uiLobby.SetActiveJoinBtn(true);
        }


    }
    public override void OnDisconnected(DisconnectCause cause)
    {

        print("Disconect From Photon Server");
        print(cause);

        isConnecting = false;
        chattingServer.LeaveChannel();
        mainUI.uiLobby.SetActiveJoinBtn(true);
    }
    public override void OnLeftRoom()
    {
        LeaveRoom();
    }
    public override void OnLeftLobby()

    {
        LeaveRoom();

    }
    #endregion

    #region Public Method for Button

    public void CreateRoom()
    {
        int roomName = Random.Range(0, 1000);
        PhotonNetwork.CreateRoom("room" + roomName, roomSetting);
    }
    public void ConectToServer(string userName)
    {
        PhotonNetwork.AuthValues = new AuthenticationValues(userName);
        isConnecting = true;
        if (PhotonNetwork.IsConnected)
        {
            // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings();
            mainUI.uiLobby.SendNotif("Connecting to Game Server", Color.yellow);
        }
        mainUI.uiLobby.SetActiveJoinBtn(false);
    }
    public void LeaveRoom()
    {
        SQLControl.UserLogout(UserData.userName);
        PhotonNetwork.Disconnect();
        chattingServer.LeaveChannel();

        GameControl_Vtour._instance.CallOnMainGameOver();
        mainUI.uiLobby.SetActiveJoinBtn(true);

        UserData.ClearCurrentData();

    }
    public void JoinRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }
    #endregion


}

