using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RoomSetting", menuName = "Vtour/RoomSetting")]
public class PhotonRoomSetting : ScriptableObject
{
    [SerializeField] int maxCreatedRoom;
    public int MaxCreatedRoom { get => maxCreatedRoom; }

    [SerializeField] byte maxPlayer;
    public byte MaxPlayer { get => maxPlayer; }

    [SerializeField] bool isOpen = true;
    public bool IsOpen  { get => isOpen; }

    [SerializeField] bool isVisible = true;
    public bool IsVisible { get => isVisible; }

    [SerializeField] bool isPublishUserID= true;
    public bool PublishUserId {get=> isPublishUserID; }

    [Space]
    [Range(1,20)]
    [SerializeField] int maxClientEachInterest;
    public int MaxClientEachInterest { get => maxClientEachInterest; }

}
