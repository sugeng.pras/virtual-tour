using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Photon.Pun;
using Photon.Chat;
using ExitGames.Client.Photon;

public class PhotonChatting : MonoBehaviour, IChatClientListener
{
    private ChatClient chatClient;

    string tryChannel;
    string selectedChannel;
    bool chatReady;

    [SerializeField] UIMainManager uiMain;

    GameControl_Vtour gameControl;

    private void Update()
    {
        if (chatClient != null) chatClient.Service();
    }

    private void Awake()
    {
        gameControl = FindObjectOfType<GameControl_Vtour>();
        chatClient = new ChatClient(this);
    }

    #region Public Method
    public void ConnectToChatServer(string ChannelName)
    {
        print("Try Connect To Photon Chat server");
        chatClient.AuthValues = new AuthenticationValues(UserData.userName);

        ChatAppSettings chatSetting = PhotonNetwork.PhotonServerSettings.AppSettings.GetChatSettings();
        chatClient.ConnectUsingSettings(chatSetting);

        tryChannel = ChannelName;

    }
    public void SendPublicMessage(string message)
    {
        chatClient.PublishMessage(selectedChannel, message);
    }
    public void LeaveChannel()
    {
        if (selectedChannel != null)
        {
            chatClient.Unsubscribe(new string[] { selectedChannel });
            selectedChannel = null;

            chatClient.Disconnect();

            chatReady = false;
        }
    }
    #endregion

    #region Used Chat Function
    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        if (channelName.Equals(this.selectedChannel))
        {
            int lastIndex = senders.Length - 1;
            uiMain.uiChatting.CreateChatObject(senders[lastIndex] + " : " + messages[lastIndex]);
        }
    }
    public void OnConnected()
    {
        print("Connect to Chat Server");
        int roomIndex = PhotonNetwork.CountOfRooms - 1;

        chatClient.Subscribe(tryChannel);
    }
    public void OnDisconnected()
    {
        uiMain.EnableUI_Chatting(false);
        uiMain.EnableUI_InGame(false);

        uiMain.EnableUI_Lobby(true);
        uiMain.EnableUI_Avatar(true);
    }
    public void OnSubscribed(string[] channels, bool[] results)
    {
        if (!chatReady)
            ShowChannel(tryChannel);
    }
    public void OnUnsubscribed(string[] channels)
    {
        uiMain.EnableUI_Chatting(false);
        uiMain.EnableUI_Lobby(true);
        uiMain.EnableUI_Avatar(true);
    }
    #endregion

    #region Unused Chat Function 

    public void DebugReturn(DebugLevel level, string message)
    {

    }
    public void OnChatStateChange(ChatState state)
    {

    }
    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        throw new System.NotImplementedException();
    }
    public void OnUserSubscribed(string channel, string user)
    {
        throw new System.NotImplementedException();
    }
    public void OnUserUnsubscribed(string channel, string user)
    {
        throw new System.NotImplementedException();
    }
    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        throw new System.NotImplementedException();
    }
    #endregion

    void ShowChannel(string channelName)
    {
        if (string.IsNullOrEmpty(channelName))
        {
            return;
        }

        ChatChannel channel = null;
        bool found = this.chatClient.TryGetChannel(channelName, out channel);
        if (!found)
        {
            Debug.Log("ShowChannel failed to find channel: " + channelName);
            return;
        }

        this.selectedChannel = channelName;

        if (found)
        {
            chatReady = true;
            tryChannel = null;

            uiMain.EnableUI_Chatting(true);
        }
    }


}

