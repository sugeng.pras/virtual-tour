using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class JSHandler
{
#if !UNITY_EDITOR && UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void DownloadImage(byte[] textureByte, int byteLenght, string fileName);
    [DllImport("__Internal")]
    private static extern bool IsMobile();
    [DllImport("__Internal")]
    private static extern bool CheckLandscape();



    public static void JS_DownloadImages(byte[] textureBytes, string fileName)
    {
        DownloadImage(textureBytes, textureBytes.Length, fileName);
    }

    public static bool JS_IsMobile()
    {
        return IsMobile();
    }

    public static bool JS_IsLandscape()
    {
        return CheckLandscape();
    }
#endif
}
