using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ScreenShootHandler : MonoBehaviour
{
    [SerializeField] Camera cam;
    bool takePictNextFrame;

    private void OnPostRender()
    {
        if (takePictNextFrame)
        {
            takePictNextFrame = false;

            RenderTexture renderTexture = cam.targetTexture;
            Texture2D result = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);

            Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);

            result.ReadPixels(rect, 0, 0);
            // TO PNG
            byte[] byteArray = result.EncodeToPNG();
            var lenght = byteArray.Length;


#if !UNITY_EDITOR && UNITY_WEBGL
            JSHandler.JS_DownloadImages(byteArray, "Lampion21.png");
            // DownloadImage(byteArray, lenght , "Lampion21.png");

#elif UNITY_EDITOR
            System.IO.File.WriteAllBytes(Application.dataPath + "/ssRes.png", byteArray);
#endif

            RenderTexture.ReleaseTemporary(renderTexture);
            cam.targetTexture = null;

            Debug.Log("Screen Shoot has Take");
        }
    }

    public void TakeScreenShoot()
    {

        cam.targetTexture = RenderTexture.GetTemporary(Screen.width, Screen.height, 16);
        takePictNextFrame = true;
    }




}
