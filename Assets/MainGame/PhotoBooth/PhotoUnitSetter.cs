using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PhotoUnitSetter
{
    Hashtable _custProp = new Hashtable();

    [SerializeField] List<Transform> photoPos;
    [SerializeField] List<int> indexPlace = new List<int>();
    [SerializeField] UnitMovement unit;
    [SerializeField] Transform returnLocation;


    public bool RequestPlace(UnitMovement unit)
    {
        int[] temp_checkPos = CheckPos();
        Debug.Log(temp_checkPos.Length);

        if (temp_checkPos.Length > 0)
        {
            int ran = Random.Range(0, temp_checkPos.Length);

            SetObjectPos(unit, photoPos[temp_checkPos[ran]].position);

            unit.transform.LookAt(unit.transform.position + new Vector3(0, 2, 1));
            unit.transform.localEulerAngles = Vector3.zero;
            unit.GetComponent<UnitAnimationControl>().CallPoseAnim(temp_checkPos[ran] + 1);

            this.unit = unit;


            return true;
        }
        else
        {
            Debug.Log("Faail");
            return false;
        }
    }
    public void LeavePos()
    {
        unit.CallWarp(returnLocation.position);
        unit.GetComponent<UnitAnimationControl>().CallPoseAnim(-1);
    }

    public int[] CheckPos()
    {
        List<int> checkPos = new List<int>();

        for (int i = 0; i < photoPos.Count; i++)
        {
            Ray ray = new Ray(photoPos[i].position + new Vector3(0, 10, 0), Vector3.down);
            RaycastHit hit = new RaycastHit();
            Debug.Log("Cast");
            if (Physics.Raycast(ray, out hit, 20))
            {
                if (hit.collider.tag == "Player")
                {
                    Debug.Log("Player Get");
                }
            }
            else
            {
                checkPos.Add(i);
                Debug.Log("No Player");
            }
        }
        return checkPos.ToArray();
    }

    void SetObjectPos(UnitMovement player, Vector3 pos)
    {
        player.CallWarp(pos);
    }

}
