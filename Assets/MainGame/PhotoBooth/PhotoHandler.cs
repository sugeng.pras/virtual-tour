using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoHandler : MonoBehaviour
{
    [SerializeField] PhotoUnitSetter unitSetter;
    [SerializeField] ScreenShootHandler ssHandler;
    [SerializeField] UIMainManager mainUIMan;
    [SerializeField] GameObject photoArea;

    public void GetPhotoPlace()
    {
        if (unitSetter.RequestPlace(GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>()))
        {
            photoArea.SetActive(true);
            GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>().EnableMove(false);

            GameControl_Vtour._instance.camManager.EnableCamera_main(false);

            mainUIMan.EnableUI_PhotoBooth(true);
        }
        else
        {
            print("Failed Get Place");
        }
    }

    public void LeavePlace()
    {
        unitSetter.LeavePos();

        GameControl_Vtour._instance.camManager.EnableCamera_main(true);
        GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>().EnableMove(true);

        photoArea.SetActive(false);


        mainUIMan.EnableUI_PhotoBooth(false);
    }

    public void TakeScreenShoot()
    {
        ssHandler.TakeScreenShoot();
    }
}
