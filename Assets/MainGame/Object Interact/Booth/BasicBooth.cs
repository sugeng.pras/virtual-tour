using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBooth : BoothObject
{
    [SerializeField] string boothName;
    #region Sample variable
    public GameObject popUpUI;
    #endregion

    public override void OnEnterEvent()
    {
        popUpUI.SetActive(true);
        print("You Enter The Booth : " + boothName);
    }

    public override void OnExitEvent()
    {
        popUpUI.SetActive(false);
        print("You Exit The Booth : " + boothName);
    }

    public override void OnStayEvent()
    {
        base.OnStayEvent();
    }
}
