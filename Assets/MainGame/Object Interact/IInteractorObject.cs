using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractorObject
{
    void OnEnterEvent();

    void OnExitEvent();

    void OnStayEvent();

}