using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Net.Mail;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UIControl_Lobby : MonoBehaviour
{
    public Button joinBtn;
    public Button signUp;

    [SerializeField] TMP_InputField usernameInputField;
    [SerializeField] TMP_InputField emailInputField;
    [SerializeField] TMP_InputField companyInputField;
    [SerializeField] TMP_InputField jobInputField;

    [SerializeField] TextMeshProUGUI notifText;


    public string GetName()
    {
        string temp = usernameInputField.text;
        return temp;
    }
    public string GetEmail()
    {
        string temp = emailInputField.text;
        return temp;
    }
    public string GetCompany()
    {
        string temp = companyInputField.text;
        return temp;
    }
    public string GetJob()
    {
        string temp = jobInputField.text;
        return temp;
    }

    public void ClearForm()
    {
        usernameInputField.text = null;
        emailInputField.text = null;
        companyInputField.text = null;
        jobInputField.text = null;
    }

    public void SetActiveJoinBtn(bool isActive)
    {
        joinBtn.interactable = isActive;
    }

    public void JoinGame()
    {
        FindObjectOfType<UIMainManager>().uiLobby.joinBtn.interactable = false;
        SQLControl.Login(GetName(), null);
    }

    public void SignUp()
    {
        if (companyInputField.text.Length != 0 && jobInputField.text.Length != 0
            && CheckName() && CheckEmail()
            )
        {
            SQLControl.CreateUserData(
                usernameInputField.text,
                emailInputField.text,
                companyInputField.text,
                jobInputField.text
                );
        }
    }

    public void SendNotif(string notifMessage, Color colorText)
    {
        notifText.text = notifMessage;
        notifText.color = colorText;
    }

    bool CheckName()
    {
        return Regex.IsMatch(usernameInputField.text, "^[a-zA-Z0-9]+$") && usernameInputField.text.Length != 0;
    }
    bool CheckEmail()
    {
        if (emailInputField.text.Length == 0)
            return false;
        try
        {
            MailAddress m = new MailAddress(emailInputField.text);

            return true;
        }
        catch (FormatException)
        {
            SendNotif("wrong email format. Use Format : firstWord@secondWord.xxx ", Color.red);
            return false;
        }

    }
}

