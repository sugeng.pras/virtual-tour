using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIControl_Avatar : MonoBehaviour
{
    [SerializeField] AvatarManager avatarManager;

    public void ChooseHead(bool isNext)
    {
        avatarManager.SetHead(isNext);
        SetAvatar();
    }
    public void ChooseBody(bool isNext)
    {
        avatarManager.SetBody(isNext);
        SetAvatar();
    }
    public void ChooseTorso(bool isNext)
    {
        avatarManager.SetTorso(isNext);
        SetAvatar();
    }
    public void ChooseHair(bool isNext)
    {
        avatarManager.SetHair(isNext);
        SetAvatar();
    }

    private void Awake()
    {
        avatarManager.SaveAvatarCode(new int[] { 0, 0, 0, 0 });
    }
    void SetAvatar()
    {
        int[] avatarCode = avatarManager.GetAvatarCode();
        avatarManager.SaveAvatarCode(avatarCode);
    }
}
