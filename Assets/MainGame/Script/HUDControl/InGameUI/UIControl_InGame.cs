using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIControl_InGame : MonoBehaviour
{
    
    [SerializeField] GameObject fastTravelList;
   public void LeaveGame()
    {
        FindObjectOfType<PhotonLobby>().LeaveRoom();
    }

    public void ShowFastTravel()
    {
        fastTravelList.SetActive(!fastTravelList.activeSelf);
    }

    private void OnEnable()
    {
        int[] allScore = UserData.scoreGame;
    }
}
