using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LeaderBoardUI : MonoBehaviour
{
    [SerializeField] LDB_ScoreControl scorePerfabs;
    [SerializeField] Transform scorePlace;

    List<LDB_ScoreControl> createdScorePerfabs = new List<LDB_ScoreControl>();
    [SerializeField] List<UserGameScore> scores = new List<UserGameScore>();

    int selectedGameID;

    void CreateScore()
    {
        List<UserGameScore> temp = new List<UserGameScore>();
        temp.AddRange(scores.OrderByDescending(x => x.gameScores[selectedGameID]));

        for (int i = 0; i < temp.Count; i++)
        {
            if (i < 5)
            {
                if (createdScorePerfabs.Count <= i)
                {
                    LDB_ScoreControl perfabs = Instantiate(scorePerfabs, scorePlace);
                    createdScorePerfabs.Add(perfabs);

                    perfabs.SetScoreView(i + 1, temp[i].userName,
                        temp[i].gameScores[selectedGameID]);
                }
                else
                {
                    createdScorePerfabs[i].gameObject.SetActive(true);
                    createdScorePerfabs[i].SetScoreView(i + 1, temp[i].userName,
                        temp[i].gameScores[selectedGameID]);
                }

            }
            else
            {
                break;
            }
        }
    }

    public void ShowLeaderBoard(GameObject leaderBoardObject)
    {
        if (!leaderBoardObject.activeSelf)
        {
            SQLControl.SetScoreBoardData();
        }
        else
        {
            foreach (LDB_ScoreControl item in createdScorePerfabs)
            {
                item.gameObject.SetActive(false);
            }
        }

        leaderBoardObject.SetActive(!leaderBoardObject.activeSelf);


    }
    public void SetSelectedGameID(int gameID)
    {
        selectedGameID = gameID;
    }

    public void SetScoreValue(string[] userData)
    {
        scores.Clear();
        List<UserGameScore> temp = new List<UserGameScore>();
        foreach (var data in userData)
        {
            if (data.Length > 2) temp.Add(new UserGameScore(data, '|'));
        }
        scores = temp.ToList();

        Invoke("CreateScore", 1);
    }

    [System.Serializable]
    public class UserGameScore
    {
        public string userName;
        public int[] gameScores = new int[4];

        public int sumScore { get => gameScores[0] + gameScores[1] + gameScores[2] + gameScores[3]; }

        public UserGameScore(string phpResult, char spliter)
        {
            string[] variable = phpResult.Split(spliter);
            if (variable.Length > 2)
            {
                userName = variable[0];

                int.TryParse(variable[1], out gameScores[0]);

                int.TryParse(variable[2], out gameScores[1]);

                int.TryParse(variable[3], out gameScores[2]);

                int.TryParse(variable[4], out gameScores[3]);
            }
        }
    }
}


