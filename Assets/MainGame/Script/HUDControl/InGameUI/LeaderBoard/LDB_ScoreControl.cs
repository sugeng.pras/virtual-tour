using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LDB_ScoreControl : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI rank;
    [SerializeField] TextMeshProUGUI username;
    [SerializeField] TextMeshProUGUI score;

    public void SetScoreView(int rank, string username, int score)
    {
        this.rank.text = rank.ToString();
        this.username.text = username.ToString();

        this.score.text = score.ToString();
    }
    
}
