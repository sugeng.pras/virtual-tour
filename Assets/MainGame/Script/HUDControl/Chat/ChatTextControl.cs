using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChatTextControl : MonoBehaviour
{
    public TextMeshProUGUI textChat;
 
    public void SetText(string message)
    {
        textChat.text = message;
    }

}
