using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIControl_Chatting : MonoBehaviour
{
    public TMP_InputField chatInputField;
    //public InputField chatInputField;
    public Transform parentPlace_chatObject;
    public ChatTextControl chatTextPerfabs;
    public ScrollRect scrollrect;

    [SerializeField] PhotonChatting chatingServer;

    public void SendMessageToServer()
    {
        chatingServer.SendPublicMessage(chatInputField.text);

        chatInputField.text = null;
        GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>().canMove = true;
        chatInputField.DeactivateInputField();

    }

    public void CreateChatObject(string text)
    {
        ChatTextControl chatSend = Instantiate(chatTextPerfabs, parentPlace_chatObject, true);
        chatSend.SetText(text);

        chatSend.transform.localScale = Vector3.one;

        //verticalScrol.SetValueWithoutNotify(0);
        scrollrect.normalizedPosition = new Vector2(0, 0);

    }

    public void Minimize(GameObject target)
    {
        target.SetActive(!target.activeSelf);
        GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>().canMove = true;
    }

    private void Start()
    {
        chatInputField.onSelect.AddListener(delegate
       {
           GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>().canMove = false;
       });
        chatInputField.onDeselect.AddListener(delegate
        {
            GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>().canMove = true;
        });

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (chatInputField.gameObject.activeSelf && chatInputField.IsActive())
            {
                if (chatInputField.text.Length != 0)
                {
                    SendMessageToServer();
                }
                else
                {
                    chatInputField.ActivateInputField();
                    GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>().canMove = false;
                }
            }
        }
    }
}
