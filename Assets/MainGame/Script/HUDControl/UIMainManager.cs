using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainManager : MonoBehaviour
{
    public UIControl_Lobby uiLobby;
    public UIControl_Chatting uiChatting;
    public UIControl_Avatar uiAvatar;
    public UIControl_InGame uiInGame;

    public GameObject uiBothPhoto;


    public void EnableUI_Lobby(bool isActive)
    {
        uiLobby.gameObject.SetActive(isActive);
    }
    public void EnableUI_Chatting(bool isActive)
    {
        uiChatting.gameObject.SetActive(isActive);
    }
    public void EnableUI_Avatar(bool isActive)
    {
        uiAvatar.gameObject.SetActive(isActive);
    }
    public void EnableUI_InGame(bool isActive)
    {
        uiInGame.gameObject.SetActive(isActive);
    }

    public void EnableUI_PhotoBooth(bool isActive)
    {
        EnableUI_Chatting(!isActive);
        EnableUI_InGame(!isActive);

        uiBothPhoto.SetActive(isActive);
    }


    //Test Load Keyboard manually

    public void LoadKeyboard()
    {
        TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, true, false, false);
    }
}
