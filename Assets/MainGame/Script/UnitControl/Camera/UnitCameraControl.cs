using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Photon.Pun;

public class UnitCameraControl : MonoBehaviour
{
    [SerializeField] Transform camHolder;
    [SerializeField] CinemachineVirtualCamera vCam;

    PhotonView PV;
    Quaternion originalRotattion;

    private void Start()
    {
        originalRotattion = transform.GetChild(0).rotation;
        camHolder = transform.GetChild(0);
        PV = GetComponent<PhotonView>();
        if (PV.IsMine && vCam == null)
        {
            vCam = GameObject.Find("TourVCam").GetComponent<CinemachineVirtualCamera>();

            vCam.Follow = transform.GetChild(0);
        }
    }
    private void LateUpdate()
    {
        camHolder.rotation = originalRotattion;
    }
}
