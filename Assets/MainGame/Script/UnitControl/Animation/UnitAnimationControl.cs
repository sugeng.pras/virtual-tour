using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class UnitAnimationControl : MonoBehaviour
{
    [SerializeField] Animator animControl;

    public void CallRunAnim()
    {
        animControl.SetBool("stand", false);
        animControl.SetBool("run", true);
    }
    public void CallStandAnim()
    {
        animControl.SetBool("stand", true);
        animControl.SetBool("run", false);
    }
    public void CallPoseAnim(int poseCode)
    {
        animControl.SetInteger("pose", poseCode);
    }

    public void CallJumpAnim()
    {
        animControl.SetTrigger("jump");
        animControl.SetBool("run", false);
    }

    public void CallRoolAnim(out  float clipLength)
    {
        animControl.SetTrigger("roll");
        animControl.SetBool("run", false);
        clipLength = GetAnimLength("PlayerStand");
    }

    float GetAnimLength(string clipName)
    {
        float result = -1;
        foreach (var item in animControl.runtimeAnimatorController.animationClips)
        {
            if (item.name == clipName)
            {
                result = item.length;
            }
        }

        return result;
    }
}
