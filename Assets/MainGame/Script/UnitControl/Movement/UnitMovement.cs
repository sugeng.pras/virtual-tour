using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class UnitMovement : MonoBehaviour, INavDestReciver
{
    [SerializeField] float speed;
    [SerializeField] UnitAnimationControl animControl;
    PhotonView pV;

    [SerializeField] Rigidbody rigid;

    Vector3 curDestination;
    [SerializeField] NavMeshAgent navAgent;

    public Transform unitTransform => this.transform;
    public bool canMove = true;

    void OnEnable()
    {
        pV = GetComponent<PhotonView>();
        if (pV.IsMine)
        {
            FindObjectOfType<GameControl_Vtour>().SetPlayerObject(this.gameObject);
        }
    }

    private void Update()
    {
        //if (pV.IsMine)
        // {
        //   canMove = (GlobalGameManager.curentGameState == GameState.vtour) ;
        //        if (canMove)
        //        {
        //            if (navAgent.remainingDistance < 0.1f)
        //            {
        //                OnReachDestination();
        //            }
        //        }
        // }
    }

    public void SetNavLocation(Vector3 navLocation)
    {
        if (canMove)
        {
            if (pV.IsMine) WalkToDest(navLocation);
        }
    }

    public void CallWarp(Vector3 destination)
    {
        pV.RPC("RPC_WarpToDestination", RpcTarget.AllBuffered, destination);
        
    }

    public void EnableMove(bool isActive)
    {
        canMove = isActive;
    }

    public void SetUnitMove(bool unitMove)
    {
        if (pV.IsMine)
        {
            if (canMove && unitMove)
            {
                Walk2();
            }
            else
            {
                animControl.CallStandAnim();
            }
        }
    }


    void Walk2()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        animControl.CallRunAnim();
    }
    void WalkToDest(Vector3 destination)
    {

        if (navAgent.SetDestination(destination))
        {
            curDestination = destination;

            animControl.CallRunAnim();
        }
        else
        {
            print("fail GetDestination");
        }

    }
    void OnReachDestination()
    {
        animControl.CallStandAnim();
    }

    [PunRPC]

    void RPC_WarpToDestination(Vector3 destination)
    {
        navAgent.Warp(destination);
    }


}
