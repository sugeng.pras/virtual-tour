using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class UnitInteractControl : MonoBehaviour
{
    PhotonView PV;
    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    public List<string> interactUnitTag =
    new List<string>(){
        "booth"
    };
    private void OnTriggerEnter(Collider other)
    {
        if (PV.IsMine)
        {

            if (interactUnitTag.Contains(other.tag))
            {
                other.GetComponent<IInteractorObject>().OnEnterEvent();
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {

        if (PV.IsMine)
        {
            if (interactUnitTag.Contains(other.tag))
            {
                other.GetComponent<IInteractorObject>().OnExitEvent();
            }
        }


    }
    private void OnTriggerStay(Collider other)
    {
        if (PV.IsMine)
        {

            if (interactUnitTag.Contains(other.tag))
            {
                other.GetComponent<IInteractorObject>().OnStayEvent();
            }
        }

    }
}
