using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class AvatarSetter : MonoBehaviour
{

    [SerializeField] GameObject avatarObject;
    [SerializeField] PhotonView PV;

    private void Start()
    {
        SetAvatar();
    }
    void SetAvatar()
    {
        int[] avaDat = PV.Owner.CustomProperties.ContainsKey("avatarCode") ?
            (int[])PV.Owner.CustomProperties["avatarCode"] : new int[] { 0, 0, 0 };

        FindObjectOfType<AvatarManager>().ApplyAvatar(avatarObject, avaDat);

    }

    [PunRPC]

    public void SetAvatarRender(bool isActive)
    {
        if (!PV.IsMine)
        {
            avatarObject.SetActive(isActive);
        }
    }
}
