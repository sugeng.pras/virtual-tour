using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;
using Photon.Realtime;

public class GameControl_Vtour : MonoBehaviourPunCallbacks
{
    [SerializeField] PhotonView pv;
    //State Game

    public Action OnMainGameStart;
    public Action OnMainGameOver;

    public void CallOnMainGameStart()
    {
        if (OnMainGameStart!=null)
        {
            OnMainGameStart();
        }
    }
    public void CallOnMainGameOver()
    {
        if (OnMainGameOver != null)
        {
            OnMainGameOver();
        }
    }

    public CameraManager camManager;

    public static GameControl_Vtour _instance;

    public GameObject playerObject;

    [SerializeField] List<Transform> spawnPos = new List<Transform>();

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    public static Vector3 GetRandomPos()
    {
        return _instance.spawnPos [UnityEngine.Random.Range(0, _instance.spawnPos.Count)].position;
    }    
    public void SetPlayerObject(GameObject playerObject)
    {   
        this.playerObject = playerObject;
        pv = playerObject.GetPhotonView();
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (pv.Owner.IsMasterClient)
        {
            SQLControl.UserLogout(otherPlayer.UserId);
        }
    }
    
    #region Mini Game Loader
    public void LoadGame(int idGame)
    {
        SceneLoader.RequestLoadScene(3+idGame, UnityEngine.SceneManagement.LoadSceneMode.Additive);
        GlobalGameManager.ChangeGameState(GameState.miniGame);
    }
    #endregion

}

