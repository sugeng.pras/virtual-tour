using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;

public class FastTravel : MonoBehaviour
{
    UnitMovement unit;

    [SerializeField] Transform[] travelPoints;



    bool GetObject()
    {
        if (unit == null)
        {
            unit = GameControl_Vtour._instance.playerObject.GetComponent<UnitMovement>();
            return true;
        }
        return false;
    }

    public void TravelTo(int index)
    {
        if (GetObject() || unit != null)
        {
            unit.CallWarp(travelPoints[index].position);
        }

    }
}
